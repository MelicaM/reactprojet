import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Button } from 'react-bootstrap';

import store from '../../store';
import { removeMoney } from '../productFunctions/actions';
import { unlockProduct } from '../../actions/data';

const NewProduct = ({ money, data }) => {
  const getPricingUnlockProduct = () => {
    let item;
    let i;

    for (i = 0; i < data.length; i += 1) {
      if (!data[i].unlock) {
        item = data[i];

        break;
      }
    }

    if (!item) {
      return null;
    }

    return item.unlockPricing;
  };

  const handleUnlockProduct = () => {
    const { dispatch } = store;
    const pricingProduct = getPricingUnlockProduct();

    if ((money - pricingProduct) < 0) {
      return;
    }

    dispatch(removeMoney(pricingProduct));
    dispatch(unlockProduct());
  };

  const changeVariant = (money - getPricingUnlockProduct() < 0) ? 'secondary' : 'danger';

  return (
    <Row className="md-5">
      <Col md={12}>
        <Button onClick={handleUnlockProduct} variant={changeVariant} size="lg" block>
          {`Unlock next product for ($${getPricingUnlockProduct()})`}
        </Button>
      </Col>
    </Row>
  );
};

const mapToProps = (state) => ({
  money: state.barrePrice.money,
  data: state.data
});

export default connect(mapToProps)(NewProduct);
