import React from 'react';
import { connect } from 'react-redux';
import { Container, Row } from 'react-bootstrap';

import Product from '../product';
import ShowPrice from '../productFunctions';
import NewProduct from '../addProduct/newproduct';
import ProductList from '../productManager';

const Home = ({ data }) => (
  <Container className="p-3 mb-2 bg-primary text-white" style={{ height: '100%' }}>
    <ShowPrice />
    <ProductList />
    <Row>
      {data
        .filter((item) => item.unlock)
        .map((item) => <Product key="test" item={item} />)}
    </Row>
    <NewProduct />
  </Container>
);

const mapToProps = (state) => ({
  data: state.data
});

export default connect(mapToProps)(Home);
