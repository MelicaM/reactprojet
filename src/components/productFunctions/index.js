import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Navbar } from 'react-bootstrap';

const ShowPrice = ({ money, cerals }) => (
  <Row className="mb-5">
    <Col md={12} className="">
      <Navbar expand="lg" variant="gray" bg="white">
        <Navbar.Brand>
          <h1 className="display-3 text-success">
            <span className="badge bg-light text-dark">Adventure Capitalist Game</span>
            <img className="image" src="https://image.flaticon.com/icons/png/512/2922/2922037.png" alt={cerals} />
            {`$ ${money.toFixed(2)}`}
          </h1>
        </Navbar.Brand>
      </Navbar>
    </Col>
  </Row>
);

const mapToProps = (state) => ({
  money: state.barrePrice.money
});

export default connect(mapToProps)(ShowPrice);
