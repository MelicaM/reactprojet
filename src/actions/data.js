export const actionTypes = {
  UNLOCK_PRODUCT: 'UNLOCK_PRODUCT',
  UNLOCK_MANAGER: 'UNLOCK_MANAGER'
};

export const unlockProduct = () => ({
  type: actionTypes.UNLOCK_PRODUCT
});

export const unlockManager = () => ({
  type: actionTypes.UNLOCK_MANAGER
});
