import { combineReducers } from 'redux';

import data from './data';
import barrePrice from '../components/productFunctions/productreducer';

export default combineReducers({
  data,
  barrePrice
});
