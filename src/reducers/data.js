import { actionTypes } from '../actions/data';

const initialState = [{
  unlockManager: false,
  unlockManagerExp: 1000,
  unlock: true,
  unlockPricing: 0,
  multi: 1,
  name: 'pineaple',
  price: 2,
  timeFactory: 500,
  totalCostFactoryUnit: 1.20,
  image: 'https://image.flaticon.com/icons/png/512/135/135598.png'
}, {
  unlockManager: false,
  unlockManagerExp: 1500,
  unlock: false,
  unlockPricing: 2000,
  multi: 1,
  name: 'cup of beer',
  price: 25,
  timeFactory: 500,
  totalCostFactoryUnit: 110,
  image: 'https://image.flaticon.com/icons/png/512/135/135759.png'
}, {
  unlockManager: false,
  unlockManagerExp: 2000,
  unlock: false,
  unlockPricing: 2500,
  multi: 1,
  name: 'carrot',
  price: 5,
  timeFactory: 700,
  totalCostFactoryUnit: 250,
  image: 'https://image.flaticon.com/icons/png/512/135/135687.png'
}, {
  unlockManager: false,
  unlockManagerExp: 2500,
  unlock: false,
  unlockPricing: 3000,
  multi: 1,
  name: 'fish',
  price: 30,
  timeFactory: 800,
  totalCostFactoryUnit: 400,
  image: 'https://image.flaticon.com/icons/png/512/135/135677.png'
}, {
  unlockManager: false,
  unlockManagerExp: 5000,
  unlock: false,
  unlockPricing: 3500,
  multi: 1,
  name: 'Salad',
  price: 2.5,
  timeFactory: 1000,
  totalCostFactoryUnit: 500,
  image: 'https://image.flaticon.com/icons/png/512/135/135637.png'
}];

const unlockProduct = (state) => {
  const stateUpdated = [...state];
  const index = stateUpdated.findIndex((item) => !item.unlock);

  stateUpdated[index].unlock = true;

  return stateUpdated;
};

const unlockManager = (state) => {
  const stateUpdated = [...state];
  const index = stateUpdated.findIndex((item) => !item.unlock);

  stateUpdated[index].unlockManager = true;

  return stateUpdated;
};

const data = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UNLOCK_MANAGER:
      return unlockManager(state);
    case actionTypes.UNLOCK_PRODUCT:
      return unlockProduct(state);
    default:
      return state;
  }
};

export default data;
